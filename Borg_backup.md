﻿# BorgBackup

Utilisation de "borg" pour la sauvegarde et extraction de serveur web qui sera automatisé par "cron" .


BORG: Le répertoire de sauvegarde se situe sur un serveur alors que les scripts de sauvegarde et d'extraction se situent du côté client.

# Les prérequis et "bonne pratique"
Installation des outils :

Borg utils :
```
    sudo yum install epel-release
    sudo yum install borgbackup
```
"Bonne pratique" utils :
```
    sudo yum install emacs
    sudo yum install tree
```
## Borg initialisation

Préparation d'un répertoire de backup "Borgsaves" que l'on assigne à l'utilisateur "root" comme répertoire de sauvegarde :
```
    mkdir Borgsaves

    useradd root --create-home --home-dir Borgsaves/
    borg init --encryption=none Borgsaves/
```

## Sauvegarde avec l'outil borg backup
```
Pour la sauvegarde l'exemple nous donne :
    borg create "Repository name"::"Borg user" "/dir to save/"

Dans notre cas il nous faut:
    borg create Borgsaves::root ./web_serv/index.html
```
__________________________________________________________________

Pour l'extraction l'exemple nous donne :
```
    borg extract "--options" "Repository name"::"Borg user"
    // Nous utiliserons --list en option afin d'avoir une énumération exhaustive des fichiers extraits.
```
Dans notre cas il nous faut :
```
    borg extract --list Borgsaves::root
```
L'avantage, et que même dans le cas d'une suppression de l'entièreté du chemin de dossiers menant au fichiers sauvegardé, lors de l'extraction, borg reconstitue la totalité de la route précédemment supprimé.

## Script de Sauvegarde
```
#!/bin/bash

borg create Borgsaves::root ./web_serv/index.html
```

## Script d'extraction
```
#!/bin/bash

borg extract --list Borgsaves::root

```
